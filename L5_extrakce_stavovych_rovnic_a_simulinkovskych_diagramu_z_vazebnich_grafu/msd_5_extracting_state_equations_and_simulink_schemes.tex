\documentclass[a4paper,11pt]{article}

%\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}

\usepackage{graphicx}
\usepackage{caption}
\usepackage{subcaption}

\usepackage{hyperref}
\usepackage{url}

\usepackage{tikz}
\usetikzlibrary{arrows}
\usetikzlibrary{backgrounds}
\usetikzlibrary{decorations.pathmorphing}
\usetikzlibrary{graphs}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{calc}
\usetikzlibrary{patterns}
\usetikzlibrary{decorations.markings}
\usepackage{lettrine}
\usepackage{titlesec}
\usepackage{fancyhdr}

%\usepackage{algorithm}
%\usepackage{algorithmic}
%\usepackage[usenames]{color}
\usepackage{listings}
%\usepackage{dtsyntax}

\usepackage{siunitx}
\usepackage[siunitx,europeanresistors]{circuitikz}

\usepackage{mgbondgraph}

%========== okraje stranky ====================================

\oddsidemargin 0.5cm 
\evensidemargin 0.5cm

\topmargin 3pt
\headheight 15pt
\headsep 20pt

\textwidth 13cm

\marginparwidth 3cm 
\marginparsep 0.5cm 

\textheight 21cm
\footskip 1.5cm
 
%============== nove prikazy a prostredi ============================

\newtheorem{example}{Example}[section]

%======================================================

\definecolor{cvutdarkblue}{RGB}{0,99,168}
\definecolor{cvutlighterblue}{RGB}{112,153,204}
\definecolor{cvutlightestblue}{RGB}{217,227,245}
\definecolor{light-gray}{gray}{0.75}

\lstset{language=Matlab, basicstyle=\scriptsize, keywordstyle=\color{blue}, backgroundcolor=\color{light-gray}}

%============== zahlavi a zapati ======================

\pagestyle{fancy}

\fancyhead{}
\rhead{\bfseries State equations from bond graphs}

\fancyfoot{}
\rfoot{\thepage}
\lfoot{Lecture 5 on Modeling and Simulation of Dynamic Dystems}

\renewcommand{\headrulewidth}{0.4pt}
\renewcommand{\footrulewidth}{0.4pt}

%\titleformat*{\section}{\fontfamily{pag}\fontsize{14pt}{16pt}\selectfont}
%============== opening ================================

\begin{document}

\thispagestyle{empty}

\begin{tikzpicture}[remember picture,overlay]

\coordinate [below=1.5cm] (midpoint) at (current page.north);

\node [name=colourbar,
draw=cvutlighterblue,
anchor=base,
fill=cvutlighterblue,
text = white,
minimum width=\paperwidth,
minimum height=1cm] at (midpoint) {};

% Define the point where the logo will go
\coordinate [right=20cm] (number) at (colourbar.west);

% Set coordinate system origin
\begin{scope}[shift=(number)]
% Draw the outline
\filldraw [cvutdarkblue] (1.3,0.5) -- ++(-2,0) -- ++(-0.8,-1) -- ++(2.8,0) --cycle;
% Include the logo
\node [text=white] {\Huge{\textbf{5}}};
\end{scope}

\end{tikzpicture}

%\vspace{-0.8cm}

\noindent
{\fontfamily{pag}\fontsize{31pt}{37pt}\selectfont State equations and Simulink models from bond graphs}\\
\vspace{0.8cm}
{\fontfamily{pag}\fontsize{11pt}{11pt}\selectfont  Including over- and undercausal bond graphs}\\
\vspace{0.0cm}
\begin{flushright}{\fontfamily{pag}\fontsize{12pt}{14pt}\fontshape{it}\selectfont  Zden\v ek Hur\' ak}\\\today\end{flushright}
\vspace{0.8cm}

\lettrine{I}{n} this lecture we will introduce causality into bond graph models by means of so-called \textit{causality strokes}. Although it was exactly the acausality of bond graph methodology that allowed us to build the model of a system, we need to introduce the causality in the final stage of model development just in order to extract the state equations. Through this lecture we will finish our introduction to the modeling methodology based on power bond graphs. Namely, we will show how a state-space model can be obtained from a bond graph with the causality of all the bonds completely determined. The extracted state equations use generalized momenta and generalized displacements as the state variables. The procedure is very straightforward. However, there may be situations when the causality cannot be completely assigned to all the bonds within a graph. Either there are some conflicts and we cannot set integral causality for all the energy storage elements, in which case we talk about overdetermined causality (in short, overcausality), or it turns out that there is some freedom even after we set  integral causality for the energy storage elements (compliances and inertances) and we can choose the causality of some components arbitrarily, in which case we talk about underdetermined causality (in short, undercausality). In this lecture we will show how to deal with these. 

\section{Causality in power bond graphs}

For the purpose of building the model of the system by assembling models of elements and subsystems we took advantage of acausality of bond graphs. Nonetheless, when we finally want to extract state equations from the bond graph, we need to introduce causality in the bond graph.

\subsection{Causality for the elements---causal stroke}

Bond graph models for individual elements always relate two variables but they do not say which of the two is the input and which is the output. The only exception was an ideal source of generalized force or generalized velocity, for which the causality was included in the definition of the elements. Now we have to consider causality for other elements.

We will use a \textit{causal mark} to denote whether the generalized force was the (independent) input enforce on the element or if the generalized velocity was the input. Note that this is unrelated to the half-arrow issue, therefore you can safely disregard it, pay no attention.

\subsubsection{Resistor}

\begin{figure}[h!]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (R)[right of=A]  {$R$};
\bondCSEnd{A}{R}{$e$}{$\dot q$};
\end{tikzpicture}
\caption*{Generalized force as the input: \\$\dot q(t) = \dot q(e(t))$.}
\end{subfigure}
\hspace{0.1\textwidth}
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (R)[right of=A]  {$R$};
\bondCSStart{A}{R}{$e$}{$\dot q$};
\end{tikzpicture}
\caption*{Gen. velocity as the input: \\$e(t) = e(\dot q(t))$.}
\end{subfigure}
%\caption*{Causality for a generalized resistor}
\end{figure}

Commonly it makes no difference which causality we choose. In particular, for linear resistors we have 

\begin{figure}[h!]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (R)[right of=A]  {$R$};
\bondCSEnd{A}{R}{$e$}{$\dot q$};
\end{tikzpicture}
\caption*{Generalized force as the input: \\$\dot q(t) = \frac{e(t)}{R}$.}
\end{subfigure}
\hspace{0.1\textwidth}
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (R)[right of=A]  {$R$};
\bondCSStart{A}{R}{$e$}{$\dot q$};
\end{tikzpicture}
\caption*{Gen. velocity as the input: \\$e(t) = R\dot q(t)$.}
\end{subfigure}
%\caption*{Causality for a generalized resistor}
\end{figure} 

But for some nonlinear resistors only one of the two variants of causality can be modelled as a function of one variable on the other.

\subsubsection{Compliance}

For simplicity we only consider linear models

\begin{figure}[h!]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (R)[right of=A]  {$C$};
\bondCSEnd{A}{R}{$e$}{$\dot q$};
\end{tikzpicture}
\caption*{Generalized force as the input: \\$\dot q(t) = C \frac{\mathrm{d}}{\mathrm{d}t}e(t)$.}
\end{subfigure}
\hspace{0.1\textwidth}
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (R)[right of=A]  {$R$};
\bondCSStart{A}{R}{$e$}{$\dot q$};
\end{tikzpicture}
\caption*{Gen. velocity as the input: \\$e(t) = \frac{1}{C}q(t)=\frac{1}{C}\int \dot q(\tau)\mathrm{d}\tau$.}
\end{subfigure}
%\caption*{Causality for a generalized compliance}
\end{figure}

Apparently, the two types of causality differ significantly---the input variable needs to be either \textit{differentiated} or \textit{integrated}. We call thes two derivative and integral causality, respectively. For reasons explained later we will prefer the integral causality, that is, we will like to see the generalized velocity as the input to the compliance.

\subsubsection{Inertance}

\begin{figure}[h!]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (R)[right of=A]  {$I$};
\bondCSEnd{A}{R}{$e$}{$\dot q$};
\end{tikzpicture}
\caption*{Generalized force as the input: \\$\dot q(t) = \frac{1}{I}p(t) = \frac{1}{I}\int e(\tau)\mathrm{d}\tau$.}
\end{subfigure}
\hspace{0.1\textwidth}
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (R)[right of=A]  {$I$};
\bondCSStart{A}{R}{$e$}{$\dot q$};
\end{tikzpicture}
\caption*{Gen. velocity as the input: \\$e(t) = I \frac{\mathrm{d}\dot q(t)}{\mathrm{d}t}$.}
\end{subfigure}
%\caption*{Causality for a generalized inertance}
\end{figure}


Similarly as with compliance, here we have an integral and derivative causality too with the integral causality being a prefered option.

\subsubsection{Transformer}

\begin{figure}[h!]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (T)[right of=A]  {$T$};
\node (B)[right of=T]  {};
\bondCSEnd{A}{T}{$e_1$}{$\dot q_1$};
\bondCSEnd{T}{B}{$e_2$}{$\dot q_2$};
\end{tikzpicture}
\caption*{One possible causality for a transformer.}
\end{subfigure}
\hspace{0.1\textwidth}
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (T)[right of=A]  {$T$};
\node (B)[right of=T]  {};
\bondCSStart{A}{T}{$e_1$}{$\dot q_1$};
\bondCSStart{T}{B}{$e_2$}{$\dot q_2$};
\end{tikzpicture}
\caption*{Another possible causality for a transformer.}
\end{subfigure}
%\caption*{Causality for a generalized transformer}
\end{figure}

Since the transformer is defined as $\dot q_2 = T\dot q_1$ and $e_1 = T \dot e_2$, it is impossible to have both $e$s as the inputs. 

\subsubsection{Gyrator}

\begin{figure}[h!]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (G)[right of=A]  {$G$};
\node (B)[right of=G]  {};
\bondCSEnd{A}{G}{$e_1$}{$\dot q_1$};
\bondCSStart{G}{B}{$e_2$}{$\dot q_2$};
\end{tikzpicture}
\caption*{One possible causality for a gyrator.}
\end{subfigure}
\hspace{0.1\textwidth}
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (A) {};
\node (G)[right of=A]  {$G$};
\node (B)[right of=G]  {};
\bondCSStart{A}{G}{$e_1$}{$\dot q_1$};
\bondCSEnd{G}{B}{$e_2$}{$\dot q_2$};
\end{tikzpicture}
\caption*{Another possible causality for a gyrator.}
\end{subfigure}
%\caption*{Causality for a generalized transformer}
\end{figure}

Similar arguments as for the transformer.

\subsubsection{Type-0 junction}

\begin{figure}[h!]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (l) {};
\node (s) [right of=l] {$0$};
\node (r) [right of=s] {};
\node (b) [below of=s] {};
\node (a) [above of=s] {};
\bondCSEnd{l}{s}{$e_1$}{$\dot q_1$};
\bondCSEnd{s}{r}{$e_4$}{$\dot q_4$};
\bondCSEnd{s}{a}{$e_2$}{$\dot q_2$};
\bondCSEnd{s}{b}{$e_3$}{$\dot q_3$};
\end{tikzpicture}
\caption*{One possible causality for a Type-0 junction.}
\end{subfigure}
\hspace{0.1\textwidth}
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (l) {};
\node (s) [right of=l] {$0$};
\node (r) [right of=s] {};
\node (b) [below of=s] {};
\node (a) [above of=s] {};
\bondCSStart{l}{s}{$e_1$}{$\dot q_1$};
\bondCSStart{s}{r}{$e_4$}{$\dot q_4$};
\bondCSEnd{s}{a}{$e_2$}{$\dot q_2$};
\bondCSEnd{s}{b}{$e_3$}{$\dot q_3$};
\end{tikzpicture}
\caption*{Another possible causality for a Type-0 causality.}
\end{subfigure}
%\caption*{Causality for a generalized Type-0 junction.}
\end{figure}


The type-0 junction is characterized by a common effort. Hence there may be only one bond throuh with the effort is enforced upon the junction. Therefore there is only one bond with a causal stroke at the type-0 junction.

\subsubsection{Type-1 junction}

\begin{figure}[h!]
\centering
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (l) {};
\node (s) [right of=l] {$1$};
\node (r) [right of=s] {};
\node (b) [below of=s] {};
\node (a) [above of=s] {};
\bondCSEnd{l}{s}{$e_1$}{$\dot q_1$};
\bondCSEnd{s}{r}{$e_4$}{$\dot q_4$};
\bondCSStart{s}{a}{$e_2$}{$\dot q_2$};
\bondCSStart{s}{b}{$e_3$}{$\dot q_3$};
\end{tikzpicture}
\caption*{One possible causality for a Type-1 juction.}
\end{subfigure}
\hspace{0.1\textwidth}
\begin{subfigure}[b]{0.4\textwidth}
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (l) {};
\node (s) [right of=l] {$1$};
\node (r) [right of=s] {};
\node (b) [below of=s] {};
\node (a) [above of=s] {};
\bondCSEnd{l}{s}{$e_1$}{$\dot q_1$};
\bondCSStart{s}{r}{$e_4$}{$\dot q_4$};
\bondCSStart{s}{a}{$e_2$}{$\dot q_2$};
\bondCSEnd{s}{b}{$e_3$}{$\dot q_3$};
\end{tikzpicture}
\caption*{Another possible causality for a Type-1 junction.}
\end{subfigure}
%\caption*{Causality for a generalized Type-1 junction.}
\end{figure}

The type-1 junction is characterized by a common flow. Hence there may be only one bond throuh with the flow is enforced upon the junction. Therefore there is only one bond without a causal stroke at the type-1 junction.

\subsection{Adding the causal strokes in the bond graphs}
We start by adding the causal strokes to the sources. Then we add the causal strokes to the compliances and inertances which will guarantee the integral causality.

\subsection{Block diagrams for bond graph elements with the causality already fixed}
Finally we can actually use Simulink to implement bond graph elements.

\section{Extracting the state equations from bond graphs}

We assume here that the causality strokes were added to all bonds in the graph with no conflict and also with no freedom once the integral causality was set for the energy storage elements. If this is not the case, overcausality or undercausality occurs and we will show in the ńext sections how to solve the issue. We consider the bond graph in Fig.~\ref{fig:bond_graph_for_state_equations_extraction}

\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (Se) {$S_{e}$};
\node (J)[right of=Se]  {$1$};
\node (O)[right of=J]  {$0$};
\node (C)[right of=O]  {$C:C$};
\node (R)[below of=J]  {$R:R$};
\node (I)[below of=O]  {$I:L$};
\bondCSEnd{Se}{J}{$u_0$}{$\times$};
\bondCSEnd{J}{R}{}{};
\bondCSStart{J}{O}{}{};
\bondCSEnd{O}{I}{}{};
\bondCSStart{O}{C}{}{};
\end{tikzpicture}
\caption{Bond graph for a parallel interconnection of two electrical capacitors connected to a voltage generator.}
\label{fig:bond_graph_for_state_equations_extraction}
\end{figure}

Start by labeling the generalized effort on the bond at every generalized inertance. However, express the generalized effort as a derivative of a generalized momentum, that is, $\dot p(t)$. The generalized momentum $p(t)$ will become a state variable. Labeling its derivative in the graph will allow us reading off a state equation directly (we will be ready to do it in a while). Brown recommends circling the variable $\dot p$ in the graph for convenience. Then we can immediately write down the generalized velocity at the same bond, which is given by $p/I$. Then label the generalized velocity at a bond at every generalized compliance. Since generalized velocity has been named $\dot q$, this directly gives us the left hand side for the state equation we are going to build. In other words, the generalized displacement $q$ will be another state variable. The generalized effort at the same bond can be set to $q/C$. Then we propagate these values to the rest of the graph following the definitions of the elements 
such as junctions, transformers and resistors and being guided by the causal strokes. The result is in Fig.~\ref{fig:bond_graph_for_state_equations_extraction_completed}

\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance = 2cm, font=\small]
\node (Se) {$S_{e}$};
\node (J)[right of=Se]  {$1$};
\node (O)[right of=J]  {$0$};
\node (C)[right of=O]  {$C:C$};
\node (R)[below of=J]  {$R:R$};
\node (I)[below of=O]  {$I:L$};
\bondCSEnd{Se}{J}{$u_0$}{$\times$};
\bondCSEnd{J}{R}{$u_0-\frac{q}{C}$}{$\frac{u_0-\frac{q}{C}}{R}$};
\bondCSStart{J}{O}{$\frac{q}{C}$}{$\frac{u_0-\frac{q}{C}}{R}$};
\bondCSStart{O}{C}{$\frac{q}{C}$}{\mgcirc{$\dot q$}};
\bondCSEnd{O}{I}{\mgcirc{$\dot p$}}{$\frac{p}{I}$};
\end{tikzpicture}
\caption{Bond graph for a parallel interconnection of two electrical capacitors connected to a voltage generator.}
\label{fig:bond_graph_for_state_equations_extraction_completed}
\end{figure}

The state equations are 
\begin{align}
 \dot p(t) &= \frac{q(t)}{C},\\
 \dot q(t) &= \frac{u_0-\frac{q}{C}}{R} - \frac{p(t)}{I}. 
 \end{align}

If you want to see these in the matrix-vector form, here it is
\begin{equation}
 \begin{bmatrix}
  \dot p(t) \\ \dot q(t)
 \end{bmatrix}
=
 \begin{bmatrix}
  0 & \frac{1}{C} \\
  -\frac{1}{I} & -\frac{1}{RC}
 \end{bmatrix}
 \begin{bmatrix}
  p(t) \\ q(t)
 \end{bmatrix}
+
 \begin{bmatrix}
  0 \\ \frac{1}{R}
 \end{bmatrix}
 u_0(t).
\end{equation}


 
A comment is appropriate here. The bond graph corresponds to an RLC circuit (can you ``reverse-engineer'' the bond graph to come up with the original circuit?) and you might be surprised to see what are the state variables here. You might have expected that we will have the voltage across the pins of an capacitor and the current through the inductor as the state variables. Instead we have $p$ and $q$. First, what is the interpretation of $p$ ($q$ is easy---charge)? Well, you may want to look into your old textbooks on electronics to see that this is actually a variable called flux linkage and often denoted $\lambda(t)$ in electrical engineering literature. Second, obtaining your desired $u_L$ and $i_C$ is easy. Just attach these two output equations to your two state equations
\begin{align}
 i_L &= \frac{p}{L},\\
 u_C &= \frac{q}{C}.
\end{align}

\section{Overdetermined causality}

It may happen that the standard procedure for furnishing the bond graph with causal strokes cannot be accomplished. Consider an example of a bond graph in Fig.~\ref{fig:overcausal_bond_graph}.

\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (Sf) {$S_f: \dot q_{0}$};
\node (O)[right of=Sf]  {$0$};
\node (G)[right of=O]  {$G:G$};
\node (J)[right of=G]  {$1$};
\node (I2)[right of=J]  {$I:I_2$};
\node (I1)[above of=O]  {$I:I_1$};
\node (C)[below of=O]  {$C:C$};
\node (R)[below of=J]  {$R:R$};
\bond{Sf}{O}{$\times$}{$\dot q_0$};
\bond{O}{G}{}{};
\bond{G}{J}{}{};
\bond{J}{R}{}{};
\bond{J}{I2}{}{};
\bond{O}{I1}{}{};
\bond{O}{C}{}{};
\end{tikzpicture}
\caption{Bond graph for a system for which the task of determining the causality for all the bonds with the accumulators of energy having an integral causality cannot be accomplished.}
\label{fig:overcausal_bond_graph}
\end{figure}

If this is attempted, one runs into troubles and as a result, one of the three energy storage elements must accept derivative causality. Let us pick the element $I_2$ as the loser here. The causality is then as in Fig.~\ref{fig:overcausal_bond_graph_with_assigned_causality}. We also determined all the variables characterizing the bonds.

\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance = 2.5cm, font=\small]
\node (Sf) {$S_f \dot q_{0}$};
\node (O)[right of=Sf]  {$0$};
\node (G)[right of=O]  {$G:G$};
\node (J)[right of=G]  {$1$};
\node (I2)[right of=J]  {$I:I_2$};
\node (I1)[above of=O]  {$I:I_1$};
\node (C)[below of=O]  {$C:C$};
\node (R)[below of=J]  {$R:R$};
\bondCSStart{Sf}{O}{$\times$}{$\dot q_0$};
\bondCSEnd{O}{G}{$\frac{q}{C}$}{$\frac{R}{CG^2}q+\frac{I_2}{CG^2}\dot q$};
\bondCSStart{G}{J}{$\frac{R}{CG}q+\frac{I_2}{CG}\dot q$}{$\frac{q}{CG}$};
\bondCSStart{J}{R}{$\frac{R}{CG}q$}{$\frac{q}{CG}$};
\bondCSStart{J}{I2}{$\frac{I_2}{CG}\dot q$}{$\frac{q}{CG}$};
\bondCSEnd{O}{I1}{\mgcirc{$\dot p_1$}}{$\frac{p_1}{I_1}$};
\bondCSStart{O}{C}{$\frac{q}{C}$}{\mgcirc{$\dot q$}};
\end{tikzpicture}
\caption{Bond graph for a system for which the task of determining the causality for all the bonds with the accumulators of energy having an integral causality cannot be accomplished.}
\label{fig:overcausal_bond_graph_with_assigned_causality}
\end{figure}

Reading the state equations for $p_1$ and $q$ directly from the bond graph is straightforward

\begin{align}
 \dot p_1 &= \frac{q}{C},\\
 \dot q &= \dot q_0 - \frac{p_1}{I_1} - \frac{R}{CG^2}q - \frac{I_2}{CG^2}{\color{red}\dot q}.
\end{align}

The essence of the problem is visible on the right side of the state equation for $q$---the derivative of the state variable is now present on both sides of the differential equation! However, such problem is trivial to solve---bring all the terms with $\dot q$ on the left hand side of the equation.

\begin{equation}
 \left(1+\frac{I_2}{CG^2}\right)\dot q = \dot q_0 - \frac{p_1}{I_1} - \frac{R}{CG^2}q
\end{equation}

We obtained an implicit differential equation. In this particular case the final step is easy---simply divide both sides by $\left(1+\frac{I_2}{CG^2}\right)$ and we have a standard state space equation. In other cases, this division may be needed in matrix form. This does not change the essence of the problem, though.

We have seen that it is easily possible to resolve the causality conflict in an algebraic way. It is occasionally possible to avoid such conflicts even at the stage of building and simplifying the bond graph. Consider two electrical capacitors interconnected in parallel and to which a voltage generator is attached, see Fig.~\ref{fig:parallel_capacitors}.

\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (Se) {$S_e: u_0$};
\node (O)[right of=Se]  {$0$};
\node (C1)[right of=O]  {$C:C_1$};
\node (C2)[below of=O]  {$C:C_2$};
\bond{Se}{O}{$u_0$}{$\times$};
\bond{O}{C1}{}{};
\bond{O}{C2}{}{};
\end{tikzpicture}
\caption{Bond graph for a parallel interconnection of two electrical capacitors connected to a voltage generator.}
\label{fig:parallel_capacitors}
\end{figure}

Apparently, there is no way to assign integral causality to both capacitors (generalized compliances). However, it is possible to create an equivalent bond graph, which will only contain a single generalized compliance as in Fig~\ref{fig:single_capacitor}.

\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (Se) {$S_e: u_0$};
\node (O)[right of=Se]  {$0$};
\node (C)[right of=O]  {$C:C$};
\bond{Se}{O}{$u_0$}{$\times$};
\bond{O}{C}{}{};
\end{tikzpicture}
\caption{Bond graph with an equivalent capacitor.}
\label{fig:single_capacitor}
\end{figure}

Although it is trivial to find the value of this equivalent capacitor (looking the expression for a parallel interconnection of capacitors in some introductory electronics textbook), we will demonstrate here one versatile procedure for finding the parameters of equivalent elements. It is based on the powerful fact that the energy stored in the original system and the simplified (equivalent) one must be the same. In order to arrive at usable results, we need to express the energy as a function of the variable which will be preserved both in the new as well as the original scheme. In our case it is the voltage. Hence we are going to express the energy stored in the capacitor as a function of the voltage across the capacitor leads
\begin{equation}
 \frac{1}{2}C_1 u_0^2 + \frac{1}{2}C_2 u_0^2 = \frac{1}{2}C u_0^2,
\end{equation}
from which it follows that 
\begin{equation}
 C= C_1+C_2.
\end{equation}

This result was certainly perfectly anticipated, but let us demonstrate the power and simplicity of the procedure by considering a slightly modified situation.
\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (Se) {$S_e: u_0$};
\node (O)[right of=Se]  {$0$};
\node (C1)[right of=O]  {$C:C_1$};
\node (T)[below of=O]  {$T:T$};
\node (C2)[below of=T]  {$C:C_2$};
\bond{Se}{O}{$u_0$}{$\times$};
\bond{O}{C1}{}{};
\bond{O}{T}{}{};
\bond{T}{C2}{}{};
\end{tikzpicture}
\caption{Bond graph which we are striving to represent by a simpler bond graph with a single \textit{equivalent} compliance.}
\label{fig:parallel_capacitors_with_transformer}
\end{figure}

The procedure based on evaluation of energy gives
\begin{equation}
 \frac{1}{2}C_1 u_0^2 + \frac{1}{2}C_2 \left(\underbrace{\frac{u_0}{T}}_{u_2}\right)^2 = \frac{1}{2}C u_0^2,
\end{equation}
from which we can immediately write
\begin{equation}
 C = C_1+\frac{1}{T^2}C_2.
\end{equation}

Make sure you have mastered this procedure by finding equivalent values for moduli of compliance and inertia elements in various interconnections. This is indeed very useful. You will not have to store these formulas in your memory.

\section{Underdetermined causality}

Handling one type of causality conficts---overdetermined causality---was relatively easy. But there is yet another type of defective scenario that one can encounter, such as the one in Fig.~\ref{fig:undercausal_bond_graph}

\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (Sf) {$S_f: \dot q_0$};
\node (O)[right of=Sf]  {$0$};
\node (R2)[right of=O]  {$R:R_2$};
\node (I)[below of=O]  {$I:I$};
\node (T)[above of=O]  {$T:T$};
\node (J)[above of=T]  {$1$};
\node (C)[left of=J]  {$C:C$};
\node (R1)[right of=J]  {$R:R_1$};
\bond{Sf}{O}{$\times$}{$\dot q_0$};
\bond{O}{I}{}{};
\bond{O}{R2}{}{};
\bond{O}{T}{}{};
\bond{T}{J}{}{};
\bond{J}{C}{}{};
\bond{J}{R1}{}{};
\end{tikzpicture}
\caption{Bond graph for which the causality cannot be uniquely determined after setting the causality for the flow source and the two energy storages.}
\label{fig:undercausal_bond_graph}
\end{figure} 

Apparently, after assigning the integral causality to all the energy storage elements, there is still some freedom in assigning the causality of the rest. This does not look like a problem. One possible choice can be as in Fig.~\ref{fig:undercausal_bond_graph_with_assigned_causality}

\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (Sf) {$S_f: \dot q_0$};
\node (O)[right of=Sf]  {$0$};
\node (R2)[right of=O]  {$R:R_2$};
\node (I)[below of=O]  {$I:I$};
\node (T)[above of=O]  {$T:T$};
\node (J)[above of=T]  {$1$};
\node (C)[left of=J]  {$C:C$};
\node (R1)[right of=J]  {$R:R_1$};
\bondCSStart{Sf}{O}{$\times$}{$\dot q_0$};
\bondCSEnd{O}{I}{}{};
\bondCSEnd{O}{R2}{}{};
\bondCSStart{O}{T}{}{};
\bondCSStart{T}{J}{}{};
\bondCSStart{J}{C}{}{};
\bondCSStart{J}{R1}{}{};
\end{tikzpicture}
\caption{One particular choice of causality for a bond graph from Fig.~\ref{fig:undercausal_bond_graph}.}
\label{fig:undercausal_bond_graph_with_assigned_causality}
\end{figure}

Now, even before we try to extract the state equations, let us build an block diagram (Simulink style) implementing this causal bond graph.

\begin{figure}[h!]
 \centering
 \includegraphics[width=9cm]{./figures/algebraic_loop.pdf}
 % algebraic_loop.pdf: 559x807 pixel, 72dpi, 19.72x28.47 cm, bb=0 0 559 807
 \caption{Simulink implementation of a bond graph with an algebraic loop formed by the closed path $T\rightarrow R_1 \rightarrow T \rightarrow 1/R_2$.}
 \label{fig:algebraic_loop_simulink}
\end{figure}

When the simulation is run for the Simulink diagram in Fig.~\ref{fig:algebraic_loop_simulink}, Simulink gives a warning message about the presence of algebraic loop. We will have more to say about algebraic loops in Simulink once we start discussing techniques for numerical simulation\footnote{You can read a bit about algebraic loops in Simulink on pages 3-39 to 3-80 of Simulink User's Guide \url{http://www.mathworks.com/help/pdf_doc/simulink/sl_using.pdf}}. For now, suffice to say that presence of algebraic loops presents some extra load for the solver. Mature simulation environments such as Simulink are able to cope with it, but at the expense of extra computational load. But if the computational time is acceptable, we can safely rely on the solver. If it is not, one has to think about reformulation of the model which would get rid of the algebraic loop. Another option is to avoid simulating such system in a signal-based environment and stick to some equation based environment such as Simscape or Modelica, 
which are specialized at simulating systems described by differential-algebraic equations (DAE). More on this later in the course.

Let us see now how we can extract the equations from the bond graph. The procedure recommended in \cite{brown_engineering_2006} is to add a \textit{virtual inertance} to a type-1 junction or a \textit{virtual compliance} to a type-0 junction which do not have the causality on all their bonds completely and uniquely assigned after the integral causality of the energy storage elements was assigned. By the adjective \textit{virtual} we mean that the corresponding generalized effort or flow, respectively, will be zero. This way the new virtual element does not disrupt the dynamics of the original system, and yet from the viewpoint of causality assignment every such virtual element removes one freedom. For example, in our bond graph we can add a virtual inertance to the type-1 junction. The rest is then resolved as in Fig.~\ref{fig:undercausal_bond_graph_with_virtual_inertance}

\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance = 1.5cm, font=\small]
\node (Sf) {$S_f: \dot q_0$};
\node (O)[right of=Sf]  {$0$};
\node (R2)[right of=O]  {$R:R_2$};
\node (I)[below of=O]  {$I:I$};
\node (T)[above of=O]  {$T:T$};
\node (J)[above of=T]  {$1$};
\node (C)[left of=J]  {$C:C$};
\node (R1)[right of=J]  {$R:R_1$};
\node (Iv)[above of=J] {\color{red}$I_v$};
\bondCSStart{Sf}{O}{$\times$}{$\dot q_0$};
\bondCSEnd{O}{I}{}{};
\bondCSStart{O}{R2}{}{};
\bondCSEnd{O}{T}{}{};
\bondCSEnd{T}{J}{}{};
\bondCSStart{J}{C}{}{};
\bondCSStart{J}{R1}{}{};
\bondCSEnd[color=red]{J}{Iv}{0}{$f_v$};
\end{tikzpicture}
\caption{The bond graph from Fig.~\ref{fig:undercausal_bond_graph} with a virtual inertance attached to the type-1 junction, which enables setting the causality of the rest of the graph uniquely.}
\label{fig:undercausal_bond_graph_with_virtual_inertance}
\end{figure}

In order to derive the equations from this causal bond graph, we follow the same procedure as before. First we set the $\dot p$ and $\dot q$ on the bonds with energy storage elements and then we propagate the known value into the rest of the graph. The result is in Fig.~\ref{fig:undercausal_bond_graph_with_virtual_inertance_evaluated}

\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance = 2.8cm, font=\small]
\node (Sf) {$S_f: \dot q_0$};
\node (O)[right of=Sf]  {$0$};
\node (R2)[right of=O]  {$R:R_2$};
\node (I)[below of=O]  {$I:I$};
\node (T)[above of=O]  {$T:T$};
\node (J)[above of=T]  {$1$};
\node (C)[left of=J]  {$C:C$};
\node (R1)[right of=J]  {$R:R_1$};
\node (Iv)[above of=J] {\color{red}$I_v$};
\bondCSStart{Sf}{O}{$\times$}{$\dot q_0$};
\bondCSEnd{O}{I}{\mgcirc{$\dot p$}}{$\frac{p}{I}$};
\bondCSStart{O}{R2}{\scriptsize $R_2(\dot q_0 - \frac{p}{I}-\frac{f_v}{T})$}{$\dot q_0 - \frac{p}{I}-\frac{f_v}{T}$};
\bondCSEnd{O}{T}{$R_2(\dot q_0 - \frac{p}{I}-\frac{f_v}{T})$}{$\frac{f_v}{T}$};
\bondCSEnd{T}{J}{$\frac{R_2}{T}(\dot q_0 - \frac{p}{I}-\frac{f_v}{T})$}{$f_v$};
\bondCSStart{J}{C}{$\frac{q}{C}$}{\mgcirc{$\dot q$}};
\bondCSStart{J}{R1}{$R_1f_v$}{$f_v$};
\bondCSEnd[color=red]{J}{Iv}{0}{$f_v$};
\end{tikzpicture}
\caption{The evaluated bond graph from Fig.~\ref{fig:undercausal_bond_graph} with a virtual inertance attached to the type-1 junction, which enabled setting the causality of the rest of the graph uniquely.}
\label{fig:undercausal_bond_graph_with_virtual_inertance_evaluated}
\end{figure}

The equations can now be written immediately using Fig.~\ref{fig:undercausal_bond_graph_with_virtual_inertance_evaluated}. We have three energy storage elements, but remember that one of them is just virtual, that is, the effort on its bond is equal to zero:

\begin{align}
 \dot q &=f_v,\\
 \dot p &= R_2\left(\dot q_0 - \frac{p}{I} - \frac{f_v}{T}\right),\\
 {\color{red}0} &= -\frac{q}{C} - R_1f_v + \frac{R_2}{T}\left(\dot q_0 - \frac{p}{I} - \frac{f_v}{T}\right).
\end{align}

Apparently, what we obtained is a set of two differential equation and one algebraic equation---an instance of the so-called DAE system as mentioned in the introductory lecture. In this particular (simple because linear) case it is possible to solve the third equation for $f_v$ and substitute it to the first two equations and obtain the standard state space model. In the general (nonlinear) case, however, this may not be possible and one will have to resort to techniques for DAE, whether for simulation purposes or for purposes of analysis or control design.

To summarize: when the causality is not determined uniquely in the bond graph after the integral causality of the energy storage elements (and of course the inputs of effort or flow) have been set, some problems can be anticipated. When implementing the bond graph in Simulink, the problem will exhibit itself as an algebraic loop. Since Simulink has some built-in support for these situations, warnings about algebraic loops can be ignored as long as the simulation time is acceptable. If the simulation is too slow, one then has to reconsider some simplifying assumptions made in the modeling state. Typically, some inertances or capacitances need not actually be negligible. When it is desired to derive equations from the bond graph, these actually come in the form of DAE. Such a set of differential and algebraic equations can by transformable to a set of ordinary differential equation (aka state-space model), but this transformation need not exist. Once again, one can either go back to the model and reconsider 
some modeling assumptions or it is also possible to resort to analytical and numerical tools for DAE. More on this towards the end of the course within lectures on numerical techniques.

\section{Literature}
In this lecture we learnt how to extract state equations directly from bond graphs. This topic is described in \cite{brown_engineering_2006} in section 3.4. The topic is then also covered by section 6.1. We also showed two classes of defective systems---systems with overdetermined causality and systems with underdetermined causality. Descriptions of these situations and some guidance on how to solve them are in section 6.2.

Alternatively, the chapter 5 in \cite{karnopp_system_2012}. Short treatment can also be found in \cite{gawthrop_bond-graph_2007}.

\bibliographystyle{plain}
\bibliography{bondgraphs}
 

\end{document}
